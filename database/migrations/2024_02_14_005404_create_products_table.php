<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->foreignId('category_id')->nullable();
            $table->string('ENVIRONMENT')->nullable();
            $table->string('TYPE_DESIGNS')->nullable();
            $table->string('CAPACITY_ENGI')->nullable();
            $table->string('PRESSURE_NOM')->nullable();
            $table->string('PRESSURE_NOM_M')->nullable();
            $table->string('RECEIVER_VOLUME')->nullable();
            $table->string('PRODUCTIVITY_NOM')->nullable();
            $table->string('PRESSURE_IN')->nullable();
            $table->string('NOISE_LEVEL')->nullable();
            $table->string('CONNECTION')->nullable();
            $table->string('CLASS_OF_PROTECTION')->nullable();
            $table->string('CURRENT_CONSUMPTION3')->nullable();
            $table->string('PRESSURE_POWER_SUP')->nullable();
            $table->string('WEIGHT')->nullable();
            $table->string('DIMENSIONS')->nullable();
            $table->string('DESCRIPTION')->nullable();
            $table->string('KEYWORDS')->nullable();
            $table->string('BIG_FOTO')->nullable();
            $table->string('MIDDLE_FOTO')->nullable();
            $table->string('SMALL_FOTO')->nullable();
            $table->string('ZAGOLOVOK')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
