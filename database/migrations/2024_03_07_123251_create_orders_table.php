<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('entity')->nullable();
            $table->string('taxpayer_number')->nullable();
            $table->string('main_state_number')->nullable();
            $table->string('legal_address')->nullable();
            $table->string('bank')->nullable();
            $table->string('bank_code')->nullable();
            $table->string('checking_account')->nullable();
            $table->boolean('delivery')->default(false)->comment('true/false');
            $table->text('delivery_address')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->text('comment')->nullable();
            $table->boolean('call_back')->default(false)->comment('true/false');
            $table->string('total')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
