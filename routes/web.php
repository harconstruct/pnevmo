<?php

use App\Http\Controllers\CatalogController;
use App\Http\Controllers\ExportImportController;
use App\Http\Controllers\OrderCallController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', [OrderCallController::class, 'orderCall']);

Route::get('/', [PagesController::class, 'index'])->name('pages.index');
Route::get('/airsystem-project', [PagesController::class, 'project'])->name('pages.project');
Route::get('/designing-and-installation', [PagesController::class, 'montazh'])->name('pages.montazh');
Route::get('/equipment-rent', [PagesController::class, 'rent'])->name('pages.rent');
Route::get('/service-and-repair', [PagesController::class, 'service'])->name('pages.service');
Route::get('/cart', [PagesController::class, 'cart'])->name('pages.cart');
Route::post('/cart/products', [PagesController::class, 'cartGetProducts']);
Route::get('/search', [PagesController::class, 'search'])->name('pages.search');

Route::post('/order', [OrderController::class, 'order'])->name('order');

Route::post('/import', [ExportImportController::class, 'import'])->name('import');
Route::post('/export', [ExportImportController::class, 'export'])->name('export');


Route::get('/catalog/{slug}/', [CatalogController::class, 'index'])->name('catalog.id');
Route::get('/catalog/{slug}/{id}', [CatalogController::class, 'child'])->name('catalog.child.name');

Route::post('/order-call', [OrderCallController::class, 'orderCall'])->name('order-call');

require __DIR__.'/auth.php';


Route::prefix('admin')->group(function (){
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->middleware(['auth', 'verified'])->name('dashboard');
    Route::middleware('auth')->group(function () {
        Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    });
});
