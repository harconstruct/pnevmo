<?php

use App\Imports\CategoryImport;
use App\Imports\ProductImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/csv', function (){


    $d = Excel::import(new ProductImport(), 'фильтры для компрессоров.csv');
//    Excel::queueImport(new ProductImport, 'resivers.csv');

//    (new ProductImport)->queue('cat.csv');
});
