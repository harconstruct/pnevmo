<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('front.head')
</head>
<body>

@include('front.modal')

<div class="page">
    @include('front.top-menu')
    <div class="container">
        @yield('content')
    </div>

    @include('front.footer')
</div>

@include('front.script')
@yield('js')

</body>
</html>
