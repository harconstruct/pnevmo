@extends('layouts.front')
@section('content')
    <section id="item">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-9 p0">
                    <div class="item">
                        <h2 class="cats_h2">{{ $category->name }}</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="calc_gor">
                                <form>
                                    <div class="form-group calc_gor_item">
                                        <label for="from2">Внутренний Ø, мм</label>
                                        <input
                                            type="text"
                                            id="from2"
                                            class="form-control calc_input"
                                        />
                                        <a href="#">
                                            <img src="/images/white.png" alt="" />
                                        </a>
                                    </div>
                                    <div class="form-group calc_gor_item">
                                        <label for="from2">Внешний Ø, мм</label>
                                        <input
                                            type="text"
                                            id="from2"
                                            class="form-control calc_input"
                                        />
                                        <a href="#">
                                            <img src="/images/white.png" alt="" />
                                        </a>
                                    </div>
                                    <div class="form-group calc_gor_item">
                                        <label for="from2">Толщина, мм</label>
                                        <input
                                            type="text"
                                            id="from2"
                                            class="form-control calc_input"
                                        />
                                        <a href="#">
                                            <img src="/images/white.png" alt="" />
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <p class="text-center">
                                            <button class="btn submit_podbor">Подобрать</button>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="item_text_center">
                                <p class="text_blue fz13px">
                                    На складе Компании Пневмосистема Вы всегда найдете
                                    сальники для винтовых блоков Rotorcomp, GHH Rand, Sulair,
                                    Termomeccanica, Aerzener, Atlas Copco, FSD, ABAC, Kamsan и
                                    других производителей необходимого Вам размера.
                                </p>
                                <p class="text_blue fz13px">
                                    Даже если вам необходим сальник к блоку неизвестного
                                    производителя — обращайтесь, мы постараемся решить вашу
                                    проблему.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="item_text_center">
                                <p class="text_blue fz13px">
                                    В наличии на складе в Москве сальники Rotorcomp SP №№:
                                    <br />
                                    84683, 107917, 109184, 110303, 110738, 110765, 110766,
                                    111538, 113047, 113351, 114027, 115493, N33593, N34932.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row table_row">
                        <!--Код выше закомментирован но не удален
                        это старый код для этой секции
                        изменено количество колонок для бокового меню на каждом экране
                        -->
                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2 col-3 side-menu">
                            <p class="text_blue_td text_slide fz13px p_700">
                                Внутренний Ø, мм
                            </p>
                            <p class="text_blue_td text_slide fz13px p_700">
                                Внешний Ø, мм
                            </p>
                            <p class="text_blue_td text_slide fz13px p_700">
                                Толщина Ø, мм
                            </p>
                            <p class="text_blue_td text_slide fz13px p_700">
                                Раб. давление, бар
                            </p>
                        </div>
                        <!-- Сделаны три слайдера для каждого типа экрана
                        в зависимости от экрана отображается свой слайдер с
                        определенным количеством элементов
                        создна карточка элемента, который отображается,
                        внутри него хедер, боди и футер
                        в хедере информация и картинка
                        в боди характеристики
                        в футере счетчик и кнопки
                      -->
                        <div class="col-xs-8 col-sm-8 col-md-9 col-lg-10 col-9 swiper-section">
                            <div class="swiper first-swiper swiper-initialized swiper-horizontal swiper-backface-hidden">
                                <div class="swiper-wrapper" id="swiper-wrapper" aria-live="polite" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px); transition-delay: 0ms;">

                                    @foreach($products as $key => $product)
                                        <div class="swiper-slide" style="width: 131.429px; margin-right: 25px;" role="group" aria-label=" {{$key}}/ 9">
                                            <div class="item">
                                                <div class="header table_name">
                                                    <img src="{{asset("/images/salnik.svg")}}" alt="" class="salnik">
                                                    <p class="text_blue_td fz15px">
                                                        {{$product["name"] ?? ""}}
                                                        <span>{{$product['DIMENSIONS']}}</span>
                                                    </p>
                                                </div>
                                                <div class="body">
                                                    <div class="table_img">
                                                        @if($product["BIG_FOTO"])
                                                            <img src="{{asset($product["BIG_FOTO"])}}" alt="" >
                                                        @else
                                                            <img src="{{asset("/images/cats/aycom.png")}}" alt="">
                                                        @endif
                                                    </div>
                                                    <div>
                                                        <p class="text_blue_td">10</p>
                                                    </div>
                                                    <div>
                                                        <p class="text_blue_td">14</p>
                                                    </div>
                                                    <div>
                                                        <p class="text_blue_td">11</p>
                                                    </div>
                                                    <div>
                                                        <p class="text_blue_td">14</p>
                                                    </div>
                                                </div>
                                                @include('components.add-to-card-section', ['product' => $product])
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="swiper-button-next first-swiper-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-c3108049f5fc2c438" aria-disabled="false"></div>
                                <div class="swiper-button-prev first-swiper-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-c3108049f5fc2c438" aria-disabled="true"></div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                    </div>
                    <div class="row item_text_bottom_row">
                        <div class="col-md-4">
                            <div class="item_text_bottom">
                                <p class="fz15px p_700">Если нет подходящего размера</p>
                                <p class="fz13px">
                                    Если у Вас не совпадает один из размеров сальника, лучше
                                    позвонить нашим специалистам и уточнить, поскольку
                                    сальники имеют стандартные размеры.
                                </p>
                                <p class="fz15px p_700">
                                    Производство сальников с заданными характеристиками
                                </p>
                                <p class="fz13px">
                                    Компания Пневмосистема может организовать производство
                                    специализированных сальников с заданными характеристиками.
                                    Изготовление осуществляется на современном оборудовании с
                                    использованием новейших технологий и высококачественных
                                    материалов. Образцы можно посмотреть в офисе компании,
                                    предварительно договорившись о встрече, и лично убедиться
                                    в их качестве.
                                </p>
                                <p class="fz15px p_700">Для чего применяется</p>
                                <p class="fz13px">
                                    В винтовой паре компрессора герметизирует зазор между
                                    ведущим винтом и корпусом пары. Корпус сальника стальной,
                                    с губками высокой прочности, обеспечивающими герметичность
                                    при рабочей температуре до 250 °С и давлением 25 бар, при
                                    частоте вращения вала до 10000 об/мин. Для большей
                                    надежности часто сальник винтового блока имеет несколько
                                    рабочих губок.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="item_text_bottom">
                                <p class="fz15px p_700">
                                    Особенности сальников винтового блока компрессора
                                </p>
                                <p class="fz13px">
                                    Зачастую у потребителя возникает вопрос: в чем отличие
                                    сальника от манжета? И сальник и манжета предназначены для
                                    уплотнения механизмов и предотвращения просачивания
                                    жидкостей или газов между деталями и корпусом механизма,
                                    но манжеты не рассчитаны на высокое рабочее давление. Для
                                    исправной работы винтового блока необходимо своевременно
                                    производить замену изнашивающихся элементов, поскольку со
                                    временем даже самый надежный сальник подвержен износу. В
                                    случае износа губок произойдет разгерметизация винтового
                                    блока и масло из компрессора начнет течь наружу.
                                    Последствия этого для любого технаря очевидны — перегрев
                                    винтового блока и его последующее заклинивание.
                                </p>
                                <p class="red">
                                    Автомобильные сальники ни в коем случае не нужно
                                    устанавливать на Ваше компрессорное оборудование, так как
                                    их рабочее давление всего 0,5 бар, а значит
                                    герметизировать винтовой блок они не могут.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="slider_sale">
                                <h2 class="h2_sale_slider">Вам может пригодиться</h2>

                                <div class="swiper second-swiper">
                                    <!-- Additional required wrapper -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                            <img
                                                src="/images/maslo.png"
                                                alt=""
                                                class="sliders_img"
                                            />
                                            <p class="fz13px p_500 desc_sale_slider">
                                                Масло для винтовых и роторно-пластинчатых воздушных
                                                компрессоров
                                            </p>
                                        </div>
                                        <div class="swiper-slide">
                                            <img
                                                src="/images/maslo.png"
                                                alt=""
                                                class="sliders_img"
                                            />
                                            <p class="fz13px p_500 desc_sale_slider">
                                                Масло для винтовых и роторно-пластинчатых воздушных
                                                компрессоров
                                            </p>
                                        </div>
                                    </div>

                                    <!-- If we need navigation buttons -->
                                    <div class="swiper-button-prev second-swiper-prev"></div>
                                    <div class="swiper-button-next second-swiper-next"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            let productIds = [];
            $('.product-count-card').map(function(index, element) {
                return productIds.push($(this).data('product'))
            })

            let getSession = sessionStorage.getItem("products")
            if(getSession.length){

                JSON.parse(getSession).map((obj) => {
                    console.log(obj)
                    if($.inArray(obj.product, productIds)){
                        $('.product-count-'+obj.product).val(obj.count)
                        console.log(obj.product)
                    }
                });
            }

        })
    </script>
@endsection
