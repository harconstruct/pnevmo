@extends('layouts.front')
@section('content')
    <section id="cats">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-md-6 p0">
                    <div class="cats">

                        <h2 class="cats_h2 vint_h2">{{ $category->name }}</h2>
                    </div>
                    <div class="cats_center">
                    @if($category->slug === \App\Models\Category::VINTOVOYE_KOMPRESORY)
                        <div class="accord">
                            <div class="accord_item accord_o">
                                <h2 class="accord_h2">
                                    Особенности
                                </h2>
                                <p class="accord_p"><span>Самые простые винтовые компрессоры имеют реле, которое включает и выключает двигатель при определенном давлении</span> <img src="{{asset('/images/collapse_arrow.svg')}}" alt="" class="collapse_arrow"></p>
                                <div class="cats_item" id="tab-in-1">
                                    <p class="fz13px text_blue">
                                        По конструкции принадлежат к ротационному компрессорному оборудованию. Впервые винтовая модель была запатентована в 1934 г. На сегодня агрегаты данного типа являются наиболее распространенными в своем сегменте. Этому способствует их относительно небольшая масса и компактные габариты, надежность, способность функционировать в автономном режиме, экономичность в плане потребления электроэнергии и затрат на обслуживание. Невысокий уровень вибрации позволяет монтировать такие системы без обустройства специального фундамента, как в случае с поршневыми аналогами.
                                        <img src="/images/accord_arrow.svg" alt="">
                                    </p>
                                </div>
                            </div>
                            <div class="accord_item accord_prem">
                                <h2 class="accord_h2">Преимущества и недостатки винтового компрессора</h2>
                                <p class="accord_p"><span>Самые простые винтовые компрессоры имеют реле, двигатель при определенном давлении</span> <img src="/images/collapse_arrow.svg" alt="" class="collapse_arrow"></p>
                                <div class="cats_item" id="tab-in-2">
                                    <p class="fz13px text_blue">
                                        Тест
                                        <img src="/images/accord_arrow.svg" alt="">
                                    </p>
                                </div>
                            </div>
                            <div class="accord_item accord_service">
                                <h2 class="accord_h2">Обслуживание и ремонт</h2>
                                <p class="accord_p"><span>Самые простые винтовые компрессоры имеют реле, которое включает и выключает. Компрессоры имеют реле, которое включает и выключает</span> <img src="/images/collapse_arrow.svg" alt="" class="collapse_arrow"></p>
                                <div class="cats_item" id="tab-in-3">
                                    <p class="fz13px text_blue">
                                        Тест 3
                                        <img src="/images/accord_arrow.svg" alt="">
                                    </p>
                                </div>
                            </div>
                            <div class="accord_item accord_delivery">
                                <h2 class="accord_h2">Сроки поставки и доставка</h2>
                                <p class="accord_p"><span>Самые простые винтовые компрессоры имеют реле, которое включает и выключает двигатель при определенном давлении.  Компрессоры имеют реле, которое включает и выключает</span> <img src="/images/collapse_arrow.svg" alt="" class="collapse_arrow"></p>
                                <div class="cats_item" id="tab-in-4">
                                    <p class="fz13px text_blue">
                                        Тест 4
                                        <img src="/images/accord_arrow.svg" alt="">
                                    </p>
                                </div>
                            </div>

                        </div>
                        @endif

                        <div class="calc">
                            <div class="row align-items-end">
                                <div class="col-md-4">
                                    <div class="calc_left">
                                        <p class="calc_left_p_top">Рабочее давление, бар</p>
                                        <p class="calc_left_p_bottom">Производительность, м3/мин</p>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="calc_center">
                                        <div class="form-inline">
                                            <div class="form-group calc_center_padding">
                                                <label for="from">от</label>
                                                <input type="text" id="from" class="form-control calc_input">
                                                <label for="from2">до</label>
                                                <input type="text" id="from2" class="form-control calc_input">
                                                <a href="#">
                                                    <img src="/images/white.png" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label for="from">от</label>
                                                <input type="text" id="from" class="form-control calc_input">
                                                <label for="from">до</label>
                                                <input type="text" id="from2" class="form-control calc_input">
                                                <a href="#">
                                                    <img src="/images/white.png" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="calc_right">
                                        <button class="btn submit_podbor">
                                            Подобрать
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="main_cats_items">
                            <div class="row">
                                @foreach($childrens as $child)
                                    <a href="{{ route('catalog.child.name', ['slug' => $category->slug, 'id' => $child->id]) }}" class="col-md-4">
                                        <div class="cats_items">
                                            <h2>{{ $child->name }}</h2>
                                            @if($child->image)
                                                <img src="{{asset($child->image)}}" alt="">
                                            @else
                                                <img src="{{asset('/images/cats/aycom.png')}}" alt="">
                                            @endif
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 del_pad_cats">
                    <div class="cats_right">

                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection
