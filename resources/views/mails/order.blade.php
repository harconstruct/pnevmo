
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css">
<link rel="stylesheet" href="/css/style.css">

<style>
    tr td {
  padding: 20px 40px;
  border: 1px solid black;
  display: block;
}
</style>

</head>
<body>
<table>
    <tr>
        <td>
            Имя: {{$data['name']}}
        </td>
        <td>
            Phone: {{$data['phone']}}
        </td>
        <td>
            Total: {{$data['total']}}
        </td>
        <td>
            Mail: {{$data['email']}}
        </td>
    </tr>
</table>
<table style="width:22%">
    <tr>
        <th>Продукт</th>
        <th>Каличество</th>
    </tr>
    @foreach($data['products'] as $product)
        <tr>
            <th>
                {{$product['name']}} 
            </th>
            <th>
               {{$data['count'][$product['id']]}}
            </t>
        </tr>
    @endforeach
</table>
</body>
</html>
