@extends('layouts.front')
@section('content')
    <section id="cart">
        <div class="container">
            <div class="cart_container ">
                <form action="{{route('order')}}" method="POST">
                    @method('POST')
                    @csrf
                    <div class="cart">
                        <div class="cart_content">
                            <div class="row d-flex align-items-center h-100">
                                <div class="col-12">

                                    <div class="d-flex justify-content-between align-items-center">
                                        <h1 class="cart_h1">Заказ</h1>
                                    </div>

                                    <div class="card-products-section">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="cart_contact">
                        <h2 class="cart_h2">
                            Реквизиты
                        </h2>
                        <div class="cart_contact_box">

                                <div class="form-group row">
                                    <label for="ur" class="col-sm-3 col-form-label">Юридическое лицо</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="ur" name="entity" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inn" class="col-sm-3 col-form-label">ИНН</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inn" name="taxpayer_number" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ogrn" class="col-sm-3 col-form-label">ОГРН</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="ogrn" name="main_state_number" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="uradr" class="col-sm-3 col-form-label">Юридический адрес</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="uradr" name="legal_address" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bank" class="col-sm-3 col-form-label">Банк</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="bank" name="bank" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bik" class="col-sm-3 col-form-label">БИК</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="bik" name="bank_code" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="chet" class="col-sm-3 col-form-label">Расчетный счет</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="chet" name="checking_account" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check text-right">
                                        <label class="form-check-label" id="need_delivery_l" for="need_delivery">
                                            Нужна доставка
                                        </label>
                                        <input class="form-check-input" type="checkbox" name="delivery" id="need_delivery">
                                    </div>
                                </div>
                                <div class="form-group row form-group-delivery">
                                    <label for="delivery" class="col-sm-3 col-form-label">Адрес доставки</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="delivery" name="delivery_address" rows="3"></textarea>
                                    </div>
                                </div>

                        </div>
                    </div>

                    <div class="cart_contact">
                    <h2 class="cart_h2">
                        Контактное лицо
                    </h2>
                    <div class="cart_contact_box">

                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Имя*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-sm-3 col-form-label">Телефон*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">E-mail*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="comment" class="col-sm-3 col-form-label">Комментарий</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check text-right">
                                    <label class="form-check-label" for="need_call">
                                        Нужно созвониться
                                    </label>
                                    <input class="form-check-input" type="checkbox" value="" id="need_call">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="submit">
                                    <button type="button" class="btn btn-secondary">Сохранить список</button>
                                    <button type="button" class="btn btn-secondary">Добавить из списка</button>
                                    <button type="button" class="btn btn-secondary">Загрузить список</button>
                                    <button type="submit" class="btn btn-primary">Отправить запрос</button>
                                </div>
                            </div>

                    </div>
                </div>
                </form>
            </div>
        </div>
        <span style="display: none" id="success" data-success="{{Session::get('success') ? 'true' : 'false'}}"></span>
    </section>

{{--    {{dd(Session::get('success'))}}--}}

@endsection
@section('js')
    <script>
        $(document).ready(function() {
                if($('#success').attr('data-success') === 'true'){
                    console.log(1);
                    sessionStorage.removeItem("products")
                    console.log(countProductInCard())
                }
            console.log(132)

                let getSession = sessionStorage.getItem("products")
                if(getSession !== null && getSession?.length) {
                    let data = JSON.parse(getSession)
                    $.ajax({
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/cart/products',
                        data: {data},
                        dataType: 'json',
                        success:  (res) => {
                            console.log(res)
                            let htm = ``;
                             res.map((resData) => {
                                 let filteredObjects = data.filter(obj => obj.product === JSON.stringify(resData.id))[0];
                                 let img;
                                 if(resData.SMALL_FOTO || resData.BIG_FOTO){
                                     img = `<img src="${resData.SMALL_FOTO ?? resData.BIG_FOTO}" class="img-fluid rounded-3 cart_img">`;
                                 }else{
                                     img = ` <img style="width: 100%; height: 100%" src="{{asset('/images/cats/aycom.png')}}" alt="">`;
                                 }
                                 htm = `
                                    <div class="card rounded-3">
                                        <div class="card-body">
                                            <div class="row d-flex justify-content-between align-items-center">
                                                <div class="col-md-1 col-lg-1 col-xl-1 text-center closer_box">
                                                    <a href="javascript:;" class="text-danger closer product-remove-card" data-id="${resData.id}">
                                                        <img src="/images/close_cart.png" alt="">
                                                    </a>
                                                </div>
                                                <div class="col-md-1 col-lg-1 col-xl-1 col-2">
                                                    <div class="cart_img_box">
                                                 
                                                        ${img}
                                              
                                                
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-lg-5 col-xl-5 col-10">
                                                    <p class="cart_zag">${resData.name}</p>

                                                </div>
                                                <div class="col-md-3 col-lg-2 col-xl-2 col-4">
                                                    <h5 class="mb-0 cart_price">150 р/шт</h5>
                                                </div>
                                                <div class="col-md-3 col-lg-3 col-xl-2 d-flex cart_forms col-3">
                                                    <input id="form1" min="0" name="product[${resData.id}]" value="${filteredObjects?.count}" type="number"
                                                           class="form-control form-control-sm change-product-card" data-id="${resData.id}" />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                `
                                 $('.card-products-section').append(htm)
                            })
                        },
                        error: function (data) {
                            // alert("OOP ERROR");
                        }
                    });
                }

            // let productIds = [];
            // $('.product-count-card').map(function(index, element) {
            //     return productIds.push($(this).data('product'))
            // })
            //
            // let getSession = sessionStorage.getItem("products")
            // if(getSession.length){
            //
            //     JSON.parse(getSession).map((obj) => {
            //         console.log(obj)
            //         if($.inArray(obj.product, productIds)){
            //             $('.product-count-'+obj.product).val(obj.count)
            //             console.log(obj.product)
            //         }
            //     });
            // }

        })
    </script>
@endsection
