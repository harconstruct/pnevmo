<script>
    $(window).on("load resize", function (e) {
        if (window.matchMedia('(max-width: 720px)').matches) {
            $('.page').css('transform', "scale(" + ($('body').width() / 400) + ")");
            $('.logo').attr('src', '/images/logo-mob.png');
            //$('.page').css('transform',"scale(1)");
            $('.swiper-slide-active img').attr('src', "images/slider-mob-1.png");

        } else {
            $('.page').css('transform', "scale(" + ($('body').width() / 1440) + ")");
            $('.logo').attr('src', '/images/logo.png');

        }
    });
    $(document).ready(function () {

        $(".project_item").each(function () {
            let more = $(this).find(".show_link > .show_link_span");
            let hide = $(this).find(".project_item_text_hide");
            hide.hide();
            more.click(function (e) {
                e.preventDefault();
                hide.slideToggle(500);
                more.text(more.text() == "скрыть" ? "подробнее" : "скрыть");
                $('.project_item_text').height("auto");
            });
        });


        $('#mobile-menu-button').on('click', function (event) {
            event.preventDefault();
            $(this).toggleClass('active_menu');
            $('.nav_menu').slideToggle("fast");
            $('.swiper-slide img').toggleClass('blur_img_slider');
        });

        $('#burder-open').on('click', function (event) {
            event.preventDefault();
            $('.navbar-mob').slideToggle("fast");
        });

        $('#open-catalog-mob').on('click', function (event) {
            event.preventDefault();
            $('.navbar-mob-menu').slideToggle("active_mob_menu");
        });
        $('.accord_item').on('click', function(){
            $('.accord_item').removeClass('active');
            $(this).addClass('active');
            $('.cats_right').html($(this).find('.cats_item').html())
        })
        $('.accord_item').eq(0).click();


        new Swiper('.swiper', {
            autoplay: true,
            disableOnInteraction: true,
            slidesPerView: "auto",
            autoHeight: true,
            speed: 2000,
            spaceBetween: 24,
            pagination: {
                el: '.pags',
                type: 'bullets',
                clickable: true,
            },
            breakpoints: {
                // when window width is >= 320px
                390: {
                    height: 500
                },
            }

        });
        var mySwiper = document.querySelector('.swiper').swiper
        $(".swiper").mouseenter(function() {
            mySwiper.autoplay.stop();
            // console.log('slider stopped');
        });

        $(".swiper").mouseleave(function() {
            mySwiper.autoplay.start();
            // console.log('slider started again');
        });
//Настройки для слайдера с сальниками
        new Swiper(".first-swiper", {
            slidesPerView: 7,
            spaceBetween: 24,
            loop: false,
            speed: 2000,
            navigation: {
                nextEl: ".first-swiper-next",
                prevEl: ".first-swiper-prev",
            },
            breakpoints: {
                // when window width is >= 320px
                300: {
                    slidesPerView : 2,
                    spaceBetween: 30,

                },
                700: {
                    slidesPerView: 3,
                },
                1000: {
                    slidesPerView: 7,
                }
            },
        });

    }); //end

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /*Order Call Ajax Request Start*/
    $("#order_call").on('click', function () {
        const order_call_name = $("#order_call_name");
        const order_call_phone = $("#order_call_phone");
        const order_call_message = $("#order_call_massage");
        if(!order_call_name.val()){
            order_call_name.addClass("order_call_valid")
        }
        else {
            order_call_name.removeClass("order_call_valid")
        }
        if(!order_call_phone.val()){
            order_call_phone.addClass("order_call_valid")
        }
        else {
            order_call_phone.removeClass("order_call_valid")
        }
        console.log(order_call_name.val())
        $.ajax({
            type: 'POST',
            url: '/order-call',
            dataType: 'json',
            data: {
                name: order_call_name.val(),
                phone: order_call_phone.val(),
                message: order_call_message.val(),
            },
            success: function () {
                window.location.reload();
            },
            error: function(){
                console.log(false);
            }
        });
    })
    /*Order Call Ajax Request End*/

    /*Increment And Decrement Product Start*/
    $(".increment").on("click", function (){
        let count = Number($(this).parent().parent().find('.footer-input').val()) + 1
        $(this).parent().parent().find('.footer-input').val(count)
    })

    $(".decrement").on("click", function (){
        if(Number($(this).parent().parent().find('.footer-input').val()) > 0){
            let count = Number($(this).parent().parent().find('.footer-input').val()) - 1
            $(this).parent().parent().find('.footer-input').val(count)
        }
    })
    /*Increment And Decrement Product End*/

    /*ADD TO Cart Logic START*/
    $(".add_to_cart").on("click", function () {
        swal({
            title: "Error!",
            text: "Here's my error message!",
            type: "error",
            confirmButtonText: "Cool"
        });
    })




    $(document).on('change', '.change-product-card', function (){
        let productId = $(this).attr('data-id')
        let count = $(this).val()
        card(productId, count);
    });


    $(document).on('click', '.product-button-card', function (){
        let productId = $(this).attr('data-id') // button
        let count = $(this).closest('.footer').find('.footer-input-section').find('.product-count-card').val() // input
        card(productId, count);
    })


    $(document).on('click', '.product-remove-card', function (){
        let productId = $(this).attr('data-id')
        let getSession = JSON.parse(sessionStorage.getItem("products"))
        removeCard(productId, getSession, $(this))
    })

    function card(productId, count)
    {
        // todo main

        let getSession = sessionStorage.getItem("products")

        let filteredObjects = getSession ? JSON.parse(getSession).filter(obj => obj.product === productId) : [];

        if(filteredObjects.length) {
            if(parseInt(count) === 0){
                removeCard(productId, JSON.parse(getSession))
            }else{
                updateCard(productId, count, getSession);
            }
        }else{
            createCard(productId, count, getSession);
        }
        countProductInCard()
    }



    function updateCard(productId, count, getSession)
    {
        let filteredObjects = JSON.parse(getSession).map((obj) => {
            if(obj.product === productId){
                obj.count = count
            }
           return obj
        });
        sessionStorage.setItem('products', JSON.stringify(filteredObjects));
    }

    function createCard(productId, count, getSession)
    {
        if (!getSession) {
            getSession = [];
        } else {
            getSession = JSON.parse(getSession);
        }
        getSession.push({
            product: productId,
            count: count
        });
        sessionStorage.setItem('products', JSON.stringify(getSession));
    }

    function removeCard(id, getSession, main)
    {
        if(getSession !== null && getSession?.length){
            let filterData = [];
            let data = getSession.map((obj, index) => {
                if(obj.product !== id){
                    filterData.push(obj)
                }
            });

            sessionStorage.setItem('products', JSON.stringify(filterData));
        }

        main.closest('.card').remove();
        countProductInCard()
    }

    function countProductInCard()
    {
        let getSession = sessionStorage.getItem("products")
        let countProd = 0
        if(getSession !== null && getSession?.length){
            JSON.parse(getSession).map((obj) => {
                return countProd += parseInt(obj.count)
            });
        }
        $('.num_cart').html(countProd)
        return countProd
    }

    $(document).ready(function() {
        // for start page add count
        countProductInCard()
    })


    /*ADD TO CART LOGIC END*/


    /*IMPOERT/EXPOERT LOGIC START*/
    $(document).on('click', '.download-card-product', function (){
        console.log(123)
        let getSession = sessionStorage.getItem("products")
        if(getSession !== null && getSession?.length) {
            let data = JSON.parse(getSession)
            $.ajax({
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/export',
                data: {data},
                dataType: 'json',
                success:  (res) => {
                    console.log(res)
                },
                error: function (data) {
                    // alert("OOP ERROR");
                }
            });
        }
    })

    /*IMPOERT/EXPOERT LOGIC END*/

</script>




