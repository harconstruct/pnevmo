<div class="modal fade" id="recall" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Заказать звонок</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="order_call_name" class="col-sm-3 col-form-label">Ваше имя *</label>

                        <input type="text" class="form-control" id="order_call_name">
                    </div>
                    <div class="form-group row">
                        <label for="order_call_phone" class="col-sm-3 col-form-label">Телефон *</label>
                        <input type="number" class="form-control" id="order_call_phone">
                    </div>
                    <div class="form-group row">
                        <label for="order_call_massage" class="col-sm-3 col-form-label">Сообщение</label>
                        <textarea class="form-control" id="order_call_massage" rows="3"></textarea>
                    </div>
                    <button type="button" class="btn btn-primary" id="order_call">Заказать</button>
                </form>
            </div>

        </div>
    </div>
</div>
