<div class="footer">
    <div class="footer-input-section">
        <input class="footer-input product-count-card product-count-{{$product->id}}"
               data-product="{{$product->id}}"
               type="number"
               value="0"
               min="0"
               step="1"
               name=""
               id="items-counter"
        >
        <div class="buttons-section">
            <button class="increment footer-button" data-action="increment"></button>
            <button class="decrement footer-button" data-action="decrement"></button>
        </div>
    </div>
    <button class="add-button footer-button product-button-card" data-id="{{$product->id}}">
        Добавить
    </button>
</div>
