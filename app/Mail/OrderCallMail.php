<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCallMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'))
            ->view('mails.orderCall')
            ->with('data', $this->data)
            ->subject("ЗАКАЗАТЬ ЗВОНОК");
    }
}
