<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;

class ImageService
{
    /**
     * @throws GuzzleException
     */
    public static function request($url)
    {
        if(str_ends_with($url, '.jpg') || str_ends_with($url, '.svg') || str_ends_with($url, '.png') || str_ends_with($url, '.jpeg')) {
            $url = 'https://www.airsystem.ru' . $url;
            ini_set('max_execution_time', 400);
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response = $client->request('GET', $url, [
                'sink' => public_path('category/'.basename($url))
            ]);
            return '/category/'.basename($url);
        }
        return '';

    }

}
