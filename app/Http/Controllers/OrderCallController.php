<?php
 
namespace App\Http\Controllers; 
 
use Response;
use App\Mail\OrderCallMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderCallController extends Controller
{
    public function orderCall(Request $request)
    {
        $data['name'] = $request->name ?? "";
        $data['phone'] = $request->phone ?? "";
        $data['message'] = $request->message ?? "";

        Mail::to(config('mail.to'))->send(new OrderCallMail($data));
       return Response::json(["data" => $data], 200);
    }
}
