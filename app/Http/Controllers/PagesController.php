<?php

namespace App\Http\Controllers;

use App\Http\Requests\Search\SearchRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }
    public function project()
    {
        return view('pages.project');
    }
    public function montazh()
    {
        return view('pages.montazh');
    }
    public function rent()
    {
        return view('pages.rent');
    }
    public function service()
    {
        return view('pages.service');
    }

    public function cart()
    {
        return view('cart.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartGetProducts(Request $request)
    {
        $product_list = array_column($request->data, 'product');
        $products = Product::query()
            ->select('id', 'name', 'slug', 'BIG_FOTO', 'SMALL_FOTO')
            ->findOrFail($product_list);
        return response()->json($products, 200);
    }

    public function search(SearchRequest $request)
    {
        $prosuct = Product::query()
            ->where( 'name','like' , '%'. $request->search . '%')
            ->orwhere('slug' ,  'like' , '%'. $request->search . '%');

        dd($prosuct);

//        return view('products');
    }
}
