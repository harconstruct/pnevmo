<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TheSeer\Tokenizer\Exception;
use App\Mail\OrderMail;
use App\Models\Product;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function order(Request $request)
    {
        try {
            DB::beginTransaction();
                $order = Order::query()->create([
                    'user_id'=> null,
                    'entity'=> $request->entity,
                    'taxpayer_number'=> $request->taxpayer_number,
                    'main_state_number'=> $request->main_state_number,
                    'legal_address'=> $request->legal_address,
                    'bank'=> $request->bank,
                    'bank_code'=> $request->bank_code,
                    'checking_account'=> $request->checking_account,
                    'delivery'=> $request->delivery == 'on' ? true : false,
                    'delivery_address'=> $request->delivery_address,
                    'name'=> $request->name,
                    'phone'=> $request->phone,
                    'email'=> $request->email,
                    'comment'=> $request->comment,
                    'call_back'=> $request->call_back == 'on' ? true : false,
                    'total'=> $request->total,
                    'status'=> $request->status,
                ]);

                
                foreach ($request->product as $itemId => $product){
                    if ($product > 0){
                        OrderProduct::query()->create([
                            'order_id' => $order->id,
                            'product_id' => $itemId,
                            'quantity' => $product,
                            'price' => null,
                            'status' => null,
                        ]);
                    }
                 }
            
            DB::commit();
                $data['name'] = $request->name ?? '';
                $data['phone'] = $request->phone ?? '';
                $data['email'] = $request->email ?? '';
                $data['total'] = $request->total ?? '';
                $data['products'] = Product::query()
                    ->select('id', 'name')
                    ->findOrFail(array_keys(array_diff($request->product, ["0"])))->toArray() ?? '';
                $data['count'] = $request->product ?? '';
                Mail::to(config('mail.to'))->send(new OrderMail($data));
                return redirect()->back()->with('success', 'successfully');
        }catch (Exception $err){
            DB::rollBack();
            return redirect()->back()->with('error', 'oops');
        }

    }
}
