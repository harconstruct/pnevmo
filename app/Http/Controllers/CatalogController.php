<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index($slug)
    {
        $category = Category::whereSlug($slug)->first();
        if($category->childrens()->count() === 0) {
            return view('pages.catalog-item')->with(['category' => $category, 'products' => $category->products]);
        }
//        return $category->childrens;
        return view('pages.catalog')->with(['category' => $category, 'childrens' => $category->childrens]);
    }

    public function child($slug, $id)
    {
        $parent = Category::whereSlug($slug)->first();
        $category = Category::findOrFail($id);
        return $this->index($category->slug);
    }
}
