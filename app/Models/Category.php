<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
class Category extends Model
{
    use HasSlug;

    const VINTOVOYE_KOMPRESORY = "vintovye-kompressory";

    protected $fillable = ['name', 'icon', 'content', 'parent_id', 'image', 'desc'];


    public function childrens(): HasMany
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parents(): HasMany
    {
        return $this->hasMany(Category::class, 'id', 'parent_id');
    }


    public function scopeParentsGet($query)
    {
        return Category::whereParentId(null);
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }
}
