<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use HasSlug;

    protected $fillable = [
        'category_id', 'name',
        'ENVIRONMENT',
        'TYPE_DESIGNS',
        'CAPACITY_ENGI',
        'PRESSURE_NOM',
        'PRESSURE_NOM_M',
        'PRODUCTIVITY_NOM',
        'PRESSURE_IN',
        'NOISE_LEVEL',
        'CONNECTION',
        'CLASS_OF_PROTECTION',
        'CURRENT_CONSUMPTION3',
        'PRESSURE_POWER_SUP',
        'WEIGHT',
        'DIMENSIONS',
        'DESCRIPTION',
        'KEYWORDS',
        'BIG_FOTO',
        'MIDDLE_FOTO',
        'SMALL_FOTO',
        'ZAGOLOVOK',
        'RECEIVER_VOLUME',
        ];


    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


    public function product()
    {
        return $this->hasMany(OrderProduct::class,'product_id','id');
    }
}
