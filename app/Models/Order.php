<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'entity',
        'taxpayer_number',
        'main_state_number',
        'legal_address',
        'bank',
        'bank_code',
        'checking_account',
        'delivery',
        'delivery_address',
        'name',
        'phone',
        'email',
        'comment',
        'call_back',
        'total',
        'status',
    ];


    public function product()
    {
        return $this->hasMany(OrderProduct::class,'order_id','id');
    }
}
